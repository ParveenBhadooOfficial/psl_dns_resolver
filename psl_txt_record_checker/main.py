import dns
import threading
from dns import resolver

def saveInFile(data,fileName="valid.txt"):
    file_ = open(fileName,"a")
    file_.write(data)
    file_.close()

def saveInInvalidFile(data,fileName="invalid.txt"):
    file_ = open(fileName,"a")
    file_.write(data)
    file_.close()

file = open("domains.txt","r")
domainlist = file.readlines()
file.close()

def rundnstest():
    open('valid.txt', 'w').close() #cleans file before processing
    open('invalid.txt', 'w').close() #cleans file before processing
    #threading.Timer(20000.0, rundnstest).start()
    n = 0
    for domain in domainlist:
        n+=1
        try:
            result = dns.resolver.resolve(domain.strip(), 'TXT')
            for ipval in result:
                record = ipval.to_text()
                if record.startswith('"https://github.com/publicsuffix/list/pull/'):
                    print(domain, " Exist\n")
                    saveInFile(f"{domain} - {record}\n\n")
        except:
            print(domain, " TXT Record Doesn't Exist\n")
            saveInInvalidFile(f"{domain}")

rundnstest()
