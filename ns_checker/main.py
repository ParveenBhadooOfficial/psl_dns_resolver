import dns
import threading
from dns import resolver

def saveInFile(data,fileName="valid.txt"):
    file_ = open(fileName,"a")
    file_.write(data)
    file_.close()

def saveInInvalidFile(data,fileName="invalid.txt"):
    file_ = open(fileName,"a")
    file_.write(data)
    file_.close()

file = open("domains.txt","r")
domainlist = file.readlines()
file.close()

def rundnstest():
    open('valid.txt', 'w').close()
    open('invalid.txt', 'w').close()
    #threading.Timer(20000.0, rundnstest).start()
    n = 0
    for domain in domainlist:
        n+=1
        try:
            result = dns.resolver.resolve(domain.strip(), 'NS')
            for ipval in result:
                record = ipval
                #if record != null:
                print(domain, " Exist\n")
                saveInFile(f"{domain} - {record}\n\n")
        except:
            print(domain, " NS Doesn't Exist\n")
            saveInInvalidFile(f"{domain}")

rundnstest()
